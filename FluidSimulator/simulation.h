#pragma once

#include <deque>
#include <glm\vec2.hpp>
#include <chrono>

#include "cell.h"
#include "particle.h"

class Simulation {

public:

	// time
	float t = 0;
	float n = 0;
	float dt_min = 0.001;
	float dt_max = 0.001;

	// room
	float h = 1;
	int nx = 20;
	int ny = 20;

	// physical
	float nu = 0.1;
	float rho = 1;
	float atmospheric_pressure = 0.1;
	glm::vec2 g = glm::vec2(0, -980.7); // cm/s^2

	//
	float k_cfl = 1;
	float pressure_iterations = 25;

	// functions
	Simulation();

	float calculateTimeStep();

	void update(float dt);

	void updateCells();
	void backwardsParticleTrace(float dt);
	void applyExternalForces(float dt);
	void applyViscosity(float dt);

	void calculatePressure(float dt);
	void applyPressure(float dt);

	void extrapolateFluidVelocities();
	void applySolidVelocity();

	void moveParticles(float dt);

	void applySources();
	void applyDrains();

	Cell* cell(int i, int j);
	Cell* cell(float x, float y);
	Cell* cell(int i);

	void addParticle(Particle * p);
	void addParticle(float x, float y);

	glm::vec2 velocity(float x, float y);
	float pressure(float x, float y);


	// variables
	std::vector<Particle*> particles;

	// data
	float calculateMaxVecocity();
	float maxPressure = atmospheric_pressure;
	float minPressure = atmospheric_pressure;

	float maxVelocity = 0;
	float minVelocity = 0;

	// options
	bool activeSources = false;
	bool activeDrains = false;


private:
	// functions
	glm::vec2 traceParticle(float x, float y, float dt);
	float interpolatedValue(float x, float y, int index);

	float divergence(int i, int j);
	glm::vec2 pressureGradient(int i, int j);
	void getNeighbouringCells(std::vector<Cell*> *vector, int x, int y);

	bool checkCflCondition(float dt);

	float divergenceError();

	// variables
	Cell * cells;

};