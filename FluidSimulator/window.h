#pragma once

#include <QtWidgets/QWidget>
#include "ui_window.h"
#include <string>

class Window : public QWidget {
	Q_OBJECT

public:
	Window(QWidget *parent = 0);

public slots:
	void setTime(float t);
	void setFrame(int i);
	void setTimeStep(float t);
	void setParticles(int t);

	void setVelocities(float min, float max);
	void setPressures(float min, float max);

private:
	Ui::WindowClass ui;
};