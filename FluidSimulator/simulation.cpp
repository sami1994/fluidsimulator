#include "simulation.h"
#include <Eigen/IterativeLinearSolvers>
#include <math.h>
#include <omp.h>
#include <fstream>
#include <iostream>
#include <time.h>

using namespace std;
using namespace glm;
using namespace chrono;
using namespace Eigen;

Simulation::Simulation() {
	cells = new Cell[nx * ny];

	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			if (i < k_cfl || j < k_cfl || i > nx - k_cfl - 1 || j > ny - k_cfl - 1) {
				cell(i, j)->type = Cell::SOLID;
			}
		}
	}
	updateCells();
}

float Simulation::calculateTimeStep() {
	float t = 0;
	float m = calculateMaxVecocity();
	// Euler particle paper equation 14
	//m = m + sqrt(h * max(g.x, g.y));
	if (m != 0) {
		t = (k_cfl * h / m);
	}
	t = max(t, dt_max);
	t = min(t, dt_min);
	return t;
}

void Simulation::update(float dt) {
	applySources();
	applyDrains();
	updateCells();
	backwardsParticleTrace(dt);
	applyExternalForces(dt);
	applyViscosity(dt);

	for (int i = 0; i < pressure_iterations; i++) {
		calculatePressure(dt);
		applyPressure(dt);
	}
	
	cout << "error: " << divergenceError() << endl;

	extrapolateFluidVelocities();
	applySolidVelocity();

	// set velocities
	minVelocity = INFINITY;
	maxVelocity = -INFINITY;
	for (int i = 0; i < nx * ny; i++) {
		Cell * c = cell(i);
		float minv = min(abs(c->velocity.x), abs(c->velocity.y));
		minVelocity = min(minVelocity, minv);
		float maxv = max(abs(c->velocity.x), abs(c->velocity.y));
		maxVelocity = max(maxVelocity, maxv);
	}

	t += dt;
	n++;
}

void Simulation::updateCells() {
	// set all FLUID cells to AIR
	for (int i = 0; i < nx * ny; i++) {
		if (cell(i)->type == Cell::FLUID) {
			// set all AIR
			cell(i)->type = Cell::AIR;
		}
	}
	// set FLUID cells
	for (Particle *p : particles) {
		Cell * c = cell(p->x, p->y);
		if (c->type != Cell::SOLID){
			c->type = Cell::FLUID;
		}
	}
	// set helper
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			Cell* c = cell(i, j);
			// right
			if (i < nx - 1) {
				c->typeRight = cell(i - 1, j)->type;
			}
			else {
				c->typeRight = Cell::SOLID;
			}
			// left
			if (i > 0) {
				c->typeLeft = cell(i - 1, j)->type;
			}
			else {
				c->typeLeft = Cell::SOLID;
			}
			// top
			if (j < ny - 1) {
				c->typeTop = cell(i, j + 1)->type;
			}
			else {
				c->typeTop = Cell::SOLID;
			}
			// bottom
			if (j > 0) {
				c->typeBottom = cell(i, j - 1)->type;
			}
			else {
				c->typeBottom = Cell::SOLID;
			}
		}
	}
}

void Simulation::backwardsParticleTrace(float dt) {
	if (dt == 0) {
		return;
	}
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			if (cell(i, j)->type == Cell::FLUID) {
				// backwards particle trace
				glm::vec2 vx = traceParticle(i - 0.5, j, -dt);
				cell(i, j)->newVelocity.x = velocity(vx.x, vx.y).x;
				glm::vec2 vy = traceParticle(i, j - 0.5, -dt);
				cell(i, j)->newVelocity.y = velocity(vy.x, vx.y).y;
				cell(i, j)->changed = true;
			}
		}
	}
	for (int i = 0; i < nx * ny; i++) {
		if (cell(i)->changed) {
			cell(i)->velocity = cell(i)->newVelocity;
			cell(i)->changed = false;
		}
	}
}

void Simulation::applyExternalForces(float dt) {
	if (dt != 0) {
		for (int i = 0; i < nx; i++) {
			for (int j = 0; j < nx; j++) {
				Cell * c = cell(i, j);
				vec2 f = vec2(0, 0);
				// gravity
				if (c->type == Cell::FLUID || c->typeLeft == Cell::FLUID) {
					f.x += dt * g.x;
				}
				//y
				if (c->type == Cell::FLUID || c->typeBottom == Cell::FLUID) {
					f.y += dt * g.y;
				}
				// sources
				if (activeSources) {
					if (c->function == Cell::SOURCE) {
						f += c->sourceVelocity;
					}
					else if (cell(i - 1, j)->function == Cell::SOURCE) {
						f.x += cell(i - 1, j)->sourceVelocity.x;
					}
					else if (cell(i, j - 1)->function == Cell::SOURCE) {
						f.y += cell(i, j - 1)->sourceVelocity.y;
					}
				}
				c->velocity += f;
			}
		}
	}
}

// check
void Simulation::applyViscosity(float dt) {
	if (dt == 0 || nu == 0) {
		return;
	}
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			Cell * c = cell(i, j);
			c->newVelocity = c->velocity;
			// x
			// only if x borders FLUID
			if (c->type == Cell::FLUID || c->typeLeft == Cell::FLUID) {
				float laplacian_x = 0;
				// x right
				int n_x = 0;
				if (c->type == Cell::FLUID) {
					laplacian_x += c->velocity.x;
					n_x++;
				}
				// x left
				if (c->typeLeft == Cell::FLUID) {
					laplacian_x += cell(i - 1, j)->velocity.x;
					n_x++;
				}
				// x top
				// maybe change
				if ((c->type == Cell::FLUID && c->typeTop == Cell::FLUID) || (cell(i - 1, j + 1)->type == Cell::FLUID && cell(i - 1, j + 1)->typeBottom == Cell::FLUID)) {
					laplacian_x += cell(i, j + 1)->velocity.x;
					n_x++;
				}
				// x bottom
				if ((c->type == Cell::FLUID && c->typeBottom == Cell::FLUID) || (cell(i - 1, j - 1)->type == Cell::FLUID && cell(i - 1, j - 1)->typeTop == Cell::FLUID)) {
					laplacian_x += cell(i, j - 1)->velocity.x;
					n_x++;
				}
				// setting velocity for x
				laplacian_x -= n_x * c->velocity.x;
				c->newVelocity.x = c->velocity.x + dt * nu * laplacian_x;
				c->changed = true;
			}
			// y
			// only if y borders FLUID
			if (c->type == Cell::FLUID || c->typeBottom == Cell::FLUID) {
				float laplacian_y = 0;
				int n_y = 0;
				// y right
				if ((c->type == Cell::FLUID && c->typeRight == Cell::FLUID) || (cell(i + 1, j - 1)->type == Cell::FLUID && cell(i + 1, j - 1)->typeLeft == Cell::FLUID)) {
					laplacian_y += cell(i + 1, j)->velocity.y;
					n_y++;
				}
				// y left
				if ((c->type == Cell::FLUID && c->typeLeft == Cell::FLUID) || (cell(i - 1, j - 1)->type == Cell::FLUID && cell(i - 1, j - 1)->typeTop == Cell::FLUID)) {
					laplacian_y += cell(i - 1, j)->velocity.y;
					n_y++;
				}
				// y top
				if (c->type == Cell::FLUID) {
					laplacian_y += cell(i, j + 1)->velocity.y;
					n_y++;
				}
				// y bottom
				if (c->typeBottom == Cell::FLUID) {
					laplacian_y += cell(i, j - 1)->velocity.y;
					n_y++;
				}
				laplacian_y -= n_y * c->velocity.y;
				c->newVelocity.y = c->velocity.y + dt * nu * laplacian_y;
				c->changed = true;
			}
		}
	}
	for (int i = 0; i < nx * ny; i++) {
		if (cell(i)->changed) {
			cell(i)->velocity = cell(i)->newVelocity;
			cell(i)->changed = false;
		}
	}
}

void Simulation::calculatePressure(float dt) {

	// one matrix line for each fluid cell

	// matrix A
	// diagonal entries are numbers of non-solid neighbors e.g. stuff with a pressure
	// off-diagonal entries are one depending on paper
	// - both cells are fluids
	// - both cells are non-solid


	// set ids
	int nextId = 0;
	for (int i = 0; i < nx * ny; i++) {
		if (cell(i)->type == Cell::FLUID) {
			// using layer as temporary id store
			cell(i)->id = nextId;
			nextId++;
		}
	}
	VectorXf B(nextId);
	typedef Triplet<float> T;
	vector<T> tripletList;
	// fill values
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			Cell * c = cell(i, j);
			if (c->type == Cell::FLUID) {
				// FILL A
				// using layer as temporary id store
				int id = c->id;
				int nsn = 0; // non solid neighbors of cell(i, j)
				int k = 0; // air neigbors if cell(i, j)

				vector<Cell*> neighbours;
				getNeighbouringCells(&neighbours, i, j);
				// for all neighboors
				for (Cell* w : neighbours) {
					switch (w->type)
					{
					case Cell::FLUID:
						tripletList.push_back(T(id, w->id, 1));
						nsn++;
						break;
					case Cell::SOLID:
						break;
					case Cell::AIR:
						k++;
						nsn++;
						break;
					default:
						break;
					}
				}
				tripletList.push_back(T(id, id, -nsn));
				// FILL B
				B(id) = rho * (1 / dt) * divergence(i, j) - k * atmospheric_pressure;
			}
		}
	}
	// set matrix A
	SparseMatrix<float> A(nextId, nextId);
	A.setFromTriplets(tripletList.begin(), tripletList.end());

	// solve A x p = b
	VectorXf P(nextId);
	ConjugateGradient <SparseMatrix<float>, Lower | Upper> solver;
	solver.compute(A);
	P = solver.solve(B);

	minPressure = INFINITY;
	maxPressure = -INFINITY;
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			Cell * c = cell(i, j);
			float p;
			switch (c->type) {
			case  Cell::FLUID:
				// using layer as temporary id store
				p = P[c->id];
				break;
			case Cell::AIR:
				p = atmospheric_pressure;
				break;
			default:
				p = 0;
				break;
			}
			c->pressure = p;
			minPressure = min(minPressure, p);
			maxPressure = max(maxPressure, p);
		}
	}
}

void Simulation::applyPressure(float dt) {
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			Cell * c = cell(i, j);
			c->newVelocity = c->velocity;
			bool x_border_fluid = c->type == Cell::FLUID || c->typeLeft == Cell::FLUID;
			bool x_border_solid = c->type == Cell::SOLID || c->typeLeft == Cell::SOLID;
			if (x_border_fluid && !x_border_solid) {
				float v_x = c->velocity.x - dt * (1 / rho) * pressureGradient(i, j).x;
				c->newVelocity.x = v_x;
				c->changed = true;
			}
			bool y_border_fluid = c->type == Cell::FLUID || c->typeBottom == Cell::FLUID;
			bool y_border_solid = c->type == Cell::SOLID || c->typeBottom == Cell::SOLID;
			if (y_border_fluid && !y_border_solid) {
				float v_y = c->velocity.y - dt * (1 / rho) * pressureGradient(i, j).y;
				c->newVelocity.y = v_y;
				c->changed = true;
			}
		}
	}
	for (int i = 0; i < nx * ny; i++) {
		if (cell(i)->changed) {
			cell(i)->velocity = cell(i)->newVelocity;
			cell(i)->changed = false;
		}
	}
}

// redo
void Simulation::extrapolateFluidVelocities() {
	// parallel
	for (int i = 0; i < nx * ny; i++) {
		Cell* c = cell(i);
		if (c->type == Cell::FLUID) {
			c->layer = 0;
		}
		else {
			c->layer = -1;
		}
		c->debug = false;
	}
	for (int l = 0; l < std::max(2, (int)std::ceil(k_cfl)); l++) {
		for (int i = 0; i < nx; i++) {
			for (int j = 0; j < ny; j++) {
				Cell* c = cell(i, j);
				if (c->layer < 0) {
					vector<Cell*> neigbours;
					getNeighbouringCells(&neigbours, i, j);
					// neigbour average
					glm::vec2 sum(0, 0);
					int n = 0;
					for (Cell* w : neigbours) {
						if (w->layer == l) {
							sum += w->velocity;
							n++;
						}
					}
					if (n > 0) {
						glm::vec2 v = sum / (float)n;
						c->newVelocity = c->velocity;
						if (c->type != Cell::FLUID && c->typeLeft != Cell::FLUID) {
							c->newVelocity.x = v.x;
							c->changed = true;
						}
						if (c->type != Cell::FLUID && c->typeBottom != Cell::FLUID) {
							c->newVelocity.y = v.y;
							c->changed = true;
						}
						c->layer = l + 1;
					}
				}
			}
		}
	}
	for (int i = 0; i < nx * ny; i++) {
		if (cell(i)->layer < 0) {
			cell(i)->velocity = vec2(0, 0);
		}
	}
	for (int i = 0; i < nx * ny; i++) {
		if (cell(i)->changed) {
			cell(i)->velocity = cell(i)->newVelocity;
			cell(i)->changed = false;
		}
	}
}

// check
void Simulation::applySolidVelocity() {
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			Cell * c = cell(i, j);
			// right
			if (c->type == Cell::SOLID && c->typeLeft != Cell::SOLID && c->velocity.x > 0) {
				c->velocity.x = 0;
			}
			// left
			if (c->type != Cell::SOLID && c->typeLeft == Cell::SOLID && c->velocity.x < 0) {
				c->velocity.x = 0;
			}
			// bottom
			if (c->type != Cell::SOLID && c->typeBottom == Cell::SOLID && c->velocity.y < 0) {
				c->velocity.y = 0;
			}
			// y top
			if (c->type == Cell::SOLID && c->typeBottom != Cell::SOLID && c->velocity.y > 0) {
				c->velocity.y = 0;
			}
			// check
			// between solids
			if (c->type == Cell::SOLID && c->typeLeft == Cell::SOLID) {
				c->velocity.x = 0;
			}
			if (c->type == Cell::SOLID && c->typeBottom == Cell::SOLID) {
				c->velocity.y = 0;
			}
		}
	}
}

float Simulation::calculateMaxVecocity() {
	float maxVelocity = 0;
	for (int i = 0; i < nx * ny; i++) {
		maxVelocity = std::max(maxVelocity, std::abs(cell(i)->velocity.x));
		maxVelocity = std::max(maxVelocity, std::abs(cell(i)->velocity.y));
	}
	return maxVelocity;
}

void Simulation::moveParticles(float dt) {
	for (Particle *p : particles) {
		glm::vec2 v = traceParticle(p->x, p->y, dt);
		p->oldx = p->x;
		p->oldy = p->y;
		p->x = v.x;
		p->y = v.y;
		if (p->x < 0 || p->y < 0 || p->x > nx || p->y > ny) {
			throw std::invalid_argument("particle moved outside boundries");
		}
	}
}

void Simulation::applySources() {
	if (activeSources) {
		for (int i = 0; i < nx; i++) {
			for (int j = 0; j < nx; j++) {
				Cell * c = cell(i, j);
				if (c->function == Cell::SOURCE && c->type == Cell::AIR) {
					// vielleicht mehr?
					addParticle(i, j);
				}
			}
		}
	}
}

void Simulation::applyDrains() {
	if (activeDrains) {
		remove_if(particles.begin(), particles.end(),
			[&](const Particle * p) {
			return
				cell(p->x, p->y)->function == Cell::DRAIN
				// tempor�ry
				|| cell(p->x, p->y + 1)->function == Cell::DRAIN;
		}
		);
	}
}

Cell* Simulation::cell(int i, int j) {
	return cell(i + j * nx);
}

Cell* Simulation::cell(float x, float y) {
	int i = (int)floor(x*h + 0.5);
	int j = (int)floor(y*h + 0.5);
	return cell(i, j);
}

Cell* Simulation::cell(int i) {
	return &cells[i];
}

void Simulation::addParticle(Particle* p) {
	if (p->x >= 0 && p->x <= nx - 1 && p->y >= 0 && p->y <= ny - 1) {
		particles.push_back(p);
	}
	else {
		throw std::invalid_argument("particle outside boundries");
	}
}

void Simulation::addParticle(float x, float y) {
	addParticle(new Particle(x, y));
}

vec2 Simulation::traceParticle(float x, float y, float dt) {
	// velocity at start
	vec2 v = velocity(x, y);
	// point half an euler step away
	v = velocity(x + 0.5 * dt * v.x, y + 0.5 * dt * v.y);
	// euler step with the average velocity
	v = glm::vec2(x, y) + dt * v;
	return v;
}

vec2 Simulation::velocity(float x, float y) {
	if (x < 0 || y < 0 || x > nx || y > ny) {
		throw std::invalid_argument("velocity access outside boundries");
	}
	glm::vec2 v(0, 0);
	// mine
	v.x = interpolatedValue(x * h + 0.5, y * h, 0);
	v.y = interpolatedValue(x * h, y * h + 0.5, 1);
	// paper
	//v.x = interpolatedValue(x/h, y/h - 0.5, 0);
	//v.y = interpolatedValue(x / h - 0.5, y / h, 1);
	return v;
}

float Simulation::pressure(float x, float y) {
	if (x < 0 || y < 0 || x > nx - 1 || y > ny - 1) {
		throw std::exception("pressure access outside boundries");
	}
	int i = floor(x);
	int j = floor(y);
	float p = 0;
	p += (i + 1 - x) * (j + 1 - y) * cell(i, j)->pressure;
	p += (x - i) * (j + 1 - y) * cell(i + 1, j)->pressure;
	p += (i + 1 - x) * (y - j) * cell(i, j + 1)->pressure;
	p += (x - i) * (y - j) * cell(i + 1, j + 1)->pressure;
	return p;
}

float Simulation::interpolatedValue(float x, float y, int index) {
	int i = floor(x);
	int j = floor(y);
	float value = 0;
	value += (i + 1 - x) * (j + 1 - y) * cell(i, j)->velocity[index];
	value += (x - i) * (j + 1 - y) * cell(i + 1, j)->velocity[index];
	value += (i + 1 - x) * (y - j) * cell(i, j + 1)->velocity[index];
	value += (x - i) * (y - j) * cell(i + 1, j + 1)->velocity[index];
	return value;
}

// redo
float Simulation::divergence(int i, int j) {
	Cell* c = cell(i, j);
	float divergence = 0;
	if (c->type != Cell::SOLID) {
		// x
		if (c->typeLeft != Cell::SOLID)
		{
			divergence += cell(i + 1, j)->velocity.x;
			divergence -= cell(i, j)->velocity.x;
		}
		// y
		if (c->typeTop != Cell::SOLID) {
			divergence += cell(i, j + 1)->velocity.y;
			divergence -= cell(i, j)->velocity.y;
		}
	}
	return divergence / h;
}

vec2 Simulation::pressureGradient(int i, int j) {
	glm::vec2 v;
	v.x = (cell(i, j)->pressure - cell(i - 1, j)->pressure) / h;
	v.y = (cell(i, j)->pressure - cell(i, j - 1)->pressure) / h;
	return v;
}

void Simulation::getNeighbouringCells(vector<Cell*>* v, int x, int y) {
	bool top = y < nx - 1;
	bool bottom = y > 0;
	bool right = x < nx - 1;
	bool left = x > 0;
	if (top) v->push_back(cell(x, y + 1));
	if (bottom) v->push_back(cell(x, y - 1));
	if (right) v->push_back(cell(x + 1, y));
	if (left) v->push_back(cell(x - 1, y));
}

bool Simulation::checkCflCondition(float dt) {
	for (int i = 0; i < nx * ny; i++) {
		if (max(cell(i)->velocity.x, cell(i)->velocity.y)  * dt > h * k_cfl) {
			return false;
		}
	}
	return true;
}

float Simulation::divergenceError() {
	float error = 0;
	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < nx; j++) {
			Cell * c = cell(i, j);
			if (c->type == Cell::FLUID) {
				error += abs(divergence(i, j));
			}
		}
	}
	return error;
}