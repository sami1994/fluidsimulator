#pragma once

#include <vector>
#include <glm/vec2.hpp>

#include "simulation.h"

struct Cell {

public:

	enum Type {
		SOLID,
		FLUID,
		AIR
	};

	enum Function {
		NONE,
		SOURCE,
		DRAIN
	};

	// basic
	int x = 0;
	int y = 0;
	Type type = Cell::AIR;
	glm::vec2 velocity = glm::vec2(0, 0);
	glm::vec2 newVelocity = glm::vec2(0, 0);
	float pressure = 0;

	// helper
	Type typeRight = Cell::AIR;
	Type typeLeft = Cell::AIR;
	Type typeTop = Cell::AIR;
	Type typeBottom = Cell::AIR;

	int layer = 0;

	int id = 0;
	int debug = 0;
	bool changed = false;

	// spource
	Function function = Cell::NONE;
	glm::vec2 sourceVelocity;

};