#include <QtWidgets/QApplication>
#include "window.h"
#include <string>
#include <Eigen/Core>
#include <iostream>
#include <omp.h>


#include <chrono>

using namespace std;

static const string NAME = "Fluid Simulator";

int main(int argc, char *argv[]) {
	Eigen::initParallel();

	QApplication a(argc, argv);
	Window w;

	w.setWindowTitle(QString::fromStdString(NAME));
	w.show();
	return a.exec();
}
