#pragma once

#include <QGLWidget>
#include <Qtimer>
#include <string>
#include <mutex>
#include <chrono>

#include "simulation.h"


class GLWidget : public QGLWidget {
	Q_OBJECT

public:
	explicit GLWidget(QWidget *parent = 0);

	void start();
	void stop();
		
	void initializeGL();
	void paintGL();
	void resizeGL(int w, int h);

public slots:
	void restart();
	void showGridVelocities(bool b);
	void showParticleVelocities(bool b);
	void showParticleSpeed(bool b);
	void showGridPressures(bool b);
	void showParticlePressures(bool b);
	void showGrid(bool b);

	void simTimerUpdate();
	void frameTimerUpdate();

	void play();
	void pause();
	void step();

	void sourceStatus(bool b);
	void drainStatus(bool b);

signals:
	void timeChanged(float t);
	void frameChanged(int i);
	void timeStepChanged(float t);
	void particlesChanged(int i);
	void velocitiesChanged(float min, float max);
	void pressuresChanged(float min, float max);
	
private:
	QTimer simTimer;
	QTimer frameTimer;
	Simulation * simulation;

	bool running = true;

	// options
	bool _showGridVelocities = false;
	bool _showParticleVelocities = false;
	bool _showParticleSpeed = false;
	bool _showGridPressures = false;
	bool _showParticlePressures = false;
	bool _showGrid = false;

	void update();
	void updateFrame();

	// opengl
	void fillRect(float x, float y, float w);
	void drawRect(float x, float y, float w);
	void drawParticle(float x, float y, float w);

};