/********************************************************************************
** Form generated from reading UI file 'window.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_H
#define UI_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glwidget.h"

QT_BEGIN_NAMESPACE

class Ui_WindowClass
{
public:
    QHBoxLayout *horizontalLayout;
    GLWidget *glWidget;
    QVBoxLayout *verticalLayout_7;
    QLabel *labelTime;
    QLabel *outputTime;
    QLabel *label_9;
    QLabel *outputFrame;
    QLabel *label_5;
    QLabel *outputTimeStep;
    QFrame *line;
    QLabel *label;
    QLabel *outputParticles;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_10;
    QLabel *outputMinPressure;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_11;
    QLabel *outputMaxPressure;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_6;
    QLabel *outputMinVelocity;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_7;
    QLabel *outputMaxVelocity;
    QFrame *line_5;
    QLabel *label_2;
    QCheckBox *cbxSources;
    QCheckBox *cbxDrains;
    QFrame *line_2;
    QLabel *label_3;
    QCheckBox *cbxShowGrid;
    QCheckBox *cbxShowGridVelocities;
    QHBoxLayout *horizontalLayout_5;
    QCheckBox *cbxShowGridPressures;
    QLabel *label_12;
    QLabel *label_8;
    QCheckBox *cbxShowParticleVelocities;
    QCheckBox *cbxShowParticleSpeed;
    QCheckBox *cbxShowParticlePressures;
    QFrame *line_4;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnPlay;
    QPushButton *btnPause;
    QPushButton *btnStep;
    QPushButton *btnRestart;
    QSpacerItem *verticalSpacer;
    QPushButton *btnClose;

    void setupUi(QWidget *WindowClass)
    {
        if (WindowClass->objectName().isEmpty())
            WindowClass->setObjectName(QStringLiteral("WindowClass"));
        WindowClass->resize(1032, 794);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(WindowClass->sizePolicy().hasHeightForWidth());
        WindowClass->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(WindowClass);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        glWidget = new GLWidget(WindowClass);
        glWidget->setObjectName(QStringLiteral("glWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(glWidget->sizePolicy().hasHeightForWidth());
        glWidget->setSizePolicy(sizePolicy1);
        glWidget->setMinimumSize(QSize(0, 0));
        glWidget->setMaximumSize(QSize(16777215, 16777215));
        glWidget->setSizeIncrement(QSize(1, 1));
        glWidget->setBaseSize(QSize(800, 800));

        horizontalLayout->addWidget(glWidget);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        labelTime = new QLabel(WindowClass);
        labelTime->setObjectName(QStringLiteral("labelTime"));

        verticalLayout_7->addWidget(labelTime);

        outputTime = new QLabel(WindowClass);
        outputTime->setObjectName(QStringLiteral("outputTime"));
        QFont font;
        font.setPointSize(12);
        outputTime->setFont(font);

        verticalLayout_7->addWidget(outputTime);

        label_9 = new QLabel(WindowClass);
        label_9->setObjectName(QStringLiteral("label_9"));

        verticalLayout_7->addWidget(label_9);

        outputFrame = new QLabel(WindowClass);
        outputFrame->setObjectName(QStringLiteral("outputFrame"));
        outputFrame->setFont(font);

        verticalLayout_7->addWidget(outputFrame);

        label_5 = new QLabel(WindowClass);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout_7->addWidget(label_5);

        outputTimeStep = new QLabel(WindowClass);
        outputTimeStep->setObjectName(QStringLiteral("outputTimeStep"));
        outputTimeStep->setFont(font);

        verticalLayout_7->addWidget(outputTimeStep);

        line = new QFrame(WindowClass);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_7->addWidget(line);

        label = new QLabel(WindowClass);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_7->addWidget(label);

        outputParticles = new QLabel(WindowClass);
        outputParticles->setObjectName(QStringLiteral("outputParticles"));
        outputParticles->setFont(font);

        verticalLayout_7->addWidget(outputParticles);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        label_10 = new QLabel(WindowClass);
        label_10->setObjectName(QStringLiteral("label_10"));

        verticalLayout_5->addWidget(label_10);

        outputMinPressure = new QLabel(WindowClass);
        outputMinPressure->setObjectName(QStringLiteral("outputMinPressure"));
        outputMinPressure->setFont(font);

        verticalLayout_5->addWidget(outputMinPressure);


        horizontalLayout_4->addLayout(verticalLayout_5);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_11 = new QLabel(WindowClass);
        label_11->setObjectName(QStringLiteral("label_11"));

        verticalLayout_4->addWidget(label_11);

        outputMaxPressure = new QLabel(WindowClass);
        outputMaxPressure->setObjectName(QStringLiteral("outputMaxPressure"));
        outputMaxPressure->setFont(font);

        verticalLayout_4->addWidget(outputMaxPressure);


        horizontalLayout_4->addLayout(verticalLayout_4);


        verticalLayout_7->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_6 = new QLabel(WindowClass);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout_2->addWidget(label_6);

        outputMinVelocity = new QLabel(WindowClass);
        outputMinVelocity->setObjectName(QStringLiteral("outputMinVelocity"));
        outputMinVelocity->setFont(font);

        verticalLayout_2->addWidget(outputMinVelocity);


        horizontalLayout_3->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_7 = new QLabel(WindowClass);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout_3->addWidget(label_7);

        outputMaxVelocity = new QLabel(WindowClass);
        outputMaxVelocity->setObjectName(QStringLiteral("outputMaxVelocity"));
        outputMaxVelocity->setFont(font);

        verticalLayout_3->addWidget(outputMaxVelocity);


        horizontalLayout_3->addLayout(verticalLayout_3);


        verticalLayout_7->addLayout(horizontalLayout_3);

        line_5 = new QFrame(WindowClass);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        verticalLayout_7->addWidget(line_5);

        label_2 = new QLabel(WindowClass);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_7->addWidget(label_2);

        cbxSources = new QCheckBox(WindowClass);
        cbxSources->setObjectName(QStringLiteral("cbxSources"));

        verticalLayout_7->addWidget(cbxSources);

        cbxDrains = new QCheckBox(WindowClass);
        cbxDrains->setObjectName(QStringLiteral("cbxDrains"));

        verticalLayout_7->addWidget(cbxDrains);

        line_2 = new QFrame(WindowClass);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_7->addWidget(line_2);

        label_3 = new QLabel(WindowClass);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_7->addWidget(label_3);

        cbxShowGrid = new QCheckBox(WindowClass);
        cbxShowGrid->setObjectName(QStringLiteral("cbxShowGrid"));

        verticalLayout_7->addWidget(cbxShowGrid);

        cbxShowGridVelocities = new QCheckBox(WindowClass);
        cbxShowGridVelocities->setObjectName(QStringLiteral("cbxShowGridVelocities"));

        verticalLayout_7->addWidget(cbxShowGridVelocities);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        cbxShowGridPressures = new QCheckBox(WindowClass);
        cbxShowGridPressures->setObjectName(QStringLiteral("cbxShowGridPressures"));

        horizontalLayout_5->addWidget(cbxShowGridPressures);

        label_12 = new QLabel(WindowClass);
        label_12->setObjectName(QStringLiteral("label_12"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_12->setFont(font1);
        label_12->setAutoFillBackground(false);
        label_12->setStyleSheet(QStringLiteral("color : red;"));

        horizontalLayout_5->addWidget(label_12);


        verticalLayout_7->addLayout(horizontalLayout_5);

        label_8 = new QLabel(WindowClass);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout_7->addWidget(label_8);

        cbxShowParticleVelocities = new QCheckBox(WindowClass);
        cbxShowParticleVelocities->setObjectName(QStringLiteral("cbxShowParticleVelocities"));

        verticalLayout_7->addWidget(cbxShowParticleVelocities);

        cbxShowParticleSpeed = new QCheckBox(WindowClass);
        cbxShowParticleSpeed->setObjectName(QStringLiteral("cbxShowParticleSpeed"));

        verticalLayout_7->addWidget(cbxShowParticleSpeed);

        cbxShowParticlePressures = new QCheckBox(WindowClass);
        cbxShowParticlePressures->setObjectName(QStringLiteral("cbxShowParticlePressures"));

        verticalLayout_7->addWidget(cbxShowParticlePressures);

        line_4 = new QFrame(WindowClass);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        verticalLayout_7->addWidget(line_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        btnPlay = new QPushButton(WindowClass);
        btnPlay->setObjectName(QStringLiteral("btnPlay"));

        horizontalLayout_2->addWidget(btnPlay);

        btnPause = new QPushButton(WindowClass);
        btnPause->setObjectName(QStringLiteral("btnPause"));

        horizontalLayout_2->addWidget(btnPause);


        verticalLayout_7->addLayout(horizontalLayout_2);

        btnStep = new QPushButton(WindowClass);
        btnStep->setObjectName(QStringLiteral("btnStep"));

        verticalLayout_7->addWidget(btnStep);

        btnRestart = new QPushButton(WindowClass);
        btnRestart->setObjectName(QStringLiteral("btnRestart"));

        verticalLayout_7->addWidget(btnRestart);

        verticalSpacer = new QSpacerItem(40, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        btnClose = new QPushButton(WindowClass);
        btnClose->setObjectName(QStringLiteral("btnClose"));
        btnClose->setMinimumSize(QSize(230, 0));

        verticalLayout_7->addWidget(btnClose);


        horizontalLayout->addLayout(verticalLayout_7);

        QWidget::setTabOrder(btnPlay, btnPause);

        retranslateUi(WindowClass);
        QObject::connect(cbxSources, SIGNAL(clicked(bool)), glWidget, SLOT(sourceStatus(bool)));
        QObject::connect(cbxDrains, SIGNAL(clicked(bool)), glWidget, SLOT(drainStatus(bool)));
        QObject::connect(cbxShowGrid, SIGNAL(clicked(bool)), glWidget, SLOT(showGrid(bool)));
        QObject::connect(cbxShowGridVelocities, SIGNAL(clicked(bool)), glWidget, SLOT(showGridVelocities(bool)));
        QObject::connect(cbxShowParticleSpeed, SIGNAL(clicked(bool)), glWidget, SLOT(showParticleSpeed(bool)));
        QObject::connect(cbxShowParticleVelocities, SIGNAL(clicked(bool)), glWidget, SLOT(showParticleVelocities(bool)));
        QObject::connect(cbxShowParticlePressures, SIGNAL(clicked(bool)), glWidget, SLOT(showParticlePressures(bool)));
        QObject::connect(btnPlay, SIGNAL(clicked()), glWidget, SLOT(play()));
        QObject::connect(btnPause, SIGNAL(clicked()), glWidget, SLOT(pause()));
        QObject::connect(btnStep, SIGNAL(clicked()), glWidget, SLOT(step()));
        QObject::connect(btnRestart, SIGNAL(clicked()), glWidget, SLOT(restart()));
        QObject::connect(btnClose, SIGNAL(clicked()), WindowClass, SLOT(close()));
        QObject::connect(cbxShowGridPressures, SIGNAL(clicked(bool)), glWidget, SLOT(showGridPressures(bool)));

        QMetaObject::connectSlotsByName(WindowClass);
    } // setupUi

    void retranslateUi(QWidget *WindowClass)
    {
        WindowClass->setWindowTitle(QApplication::translate("WindowClass", "Window", 0));
        labelTime->setText(QApplication::translate("WindowClass", "time (s):", 0));
        outputTime->setText(QApplication::translate("WindowClass", "0", 0));
        label_9->setText(QApplication::translate("WindowClass", "frame:", 0));
        outputFrame->setText(QApplication::translate("WindowClass", "0", 0));
        label_5->setText(QApplication::translate("WindowClass", "time step (s):", 0));
        outputTimeStep->setText(QApplication::translate("WindowClass", "0", 0));
        label->setText(QApplication::translate("WindowClass", "particles:", 0));
        outputParticles->setText(QApplication::translate("WindowClass", "0", 0));
        label_10->setText(QApplication::translate("WindowClass", "min. pressure", 0));
        outputMinPressure->setText(QApplication::translate("WindowClass", "0", 0));
        label_11->setText(QApplication::translate("WindowClass", "max. pressure", 0));
        outputMaxPressure->setText(QApplication::translate("WindowClass", "0", 0));
        label_6->setText(QApplication::translate("WindowClass", "min. velocity", 0));
        outputMinVelocity->setText(QApplication::translate("WindowClass", "0", 0));
        label_7->setText(QApplication::translate("WindowClass", "max. velocity", 0));
        outputMaxVelocity->setText(QApplication::translate("WindowClass", "0", 0));
        label_2->setText(QApplication::translate("WindowClass", "toggle:", 0));
        cbxSources->setText(QApplication::translate("WindowClass", "sources", 0));
        cbxDrains->setText(QApplication::translate("WindowClass", "drains", 0));
        label_3->setText(QApplication::translate("WindowClass", "grid information:", 0));
        cbxShowGrid->setText(QApplication::translate("WindowClass", "show", 0));
        cbxShowGridVelocities->setText(QApplication::translate("WindowClass", "velocities (vectors)", 0));
        cbxShowGridPressures->setText(QApplication::translate("WindowClass", "pressures (colors)", 0));
        label_12->setText(QApplication::translate("WindowClass", "Epilepsiewarnung!", 0));
        label_8->setText(QApplication::translate("WindowClass", "particle information:", 0));
        cbxShowParticleVelocities->setText(QApplication::translate("WindowClass", "velocities (vectors)", 0));
        cbxShowParticleSpeed->setText(QApplication::translate("WindowClass", "speed (colors)", 0));
        cbxShowParticlePressures->setText(QApplication::translate("WindowClass", "pressures (colors)", 0));
        btnPlay->setText(QApplication::translate("WindowClass", "play", 0));
        btnPause->setText(QApplication::translate("WindowClass", "pause", 0));
        btnStep->setText(QApplication::translate("WindowClass", "step", 0));
        btnRestart->setText(QApplication::translate("WindowClass", "restart", 0));
        btnClose->setText(QApplication::translate("WindowClass", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class WindowClass: public Ui_WindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_H
