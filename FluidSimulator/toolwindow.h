#pragma once

#include <QtWidgets/QWidget>
#include "ui_toolwindow.h"
#include <string>

class ToolWindow : public QWidget {
	Q_OBJECT

public:
	ToolWindow(QWidget *parent = 0);

private:
	Ui::WindowClass ui;
};