#pragma once

class Particle {

public:
	//functions
	inline Particle(float x, float y) {
		this->x = x;
		this->y = y;
		this->oldx = x;
		this->oldy = y;

	}

	inline Particle(Particle *p) {
		this->x = p->x;
		this->y = p->y;
		this->oldx = p->oldx;
		this->oldy = p->oldy;
	}

	// variables
	float x;
	float y;

	float oldx;
	float oldy;

	bool debug = false;

};