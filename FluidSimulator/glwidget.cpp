#include "glwidget.h"
#include <iostream>
#include <cstdlib>

using namespace std;
using namespace glm;
using namespace chrono;

static const float FPS = 60;

static const float TIME_RATIO_FROM_SECONDS = 1e+7;
static const float VELOCITY_DRAW_RATIO = 0.01;

GLWidget::GLWidget(QWidget* parent) : QGLWidget(parent) {
	connect(&simTimer, SIGNAL(timeout()), this, SLOT(simTimerUpdate()));
	connect(&frameTimer, SIGNAL(timeout()), this, SLOT(frameTimerUpdate()));
	connect(this, SIGNAL(timeChanged(float)), parentWidget(), SLOT(setTime(float)));
	connect(this, SIGNAL(frameChanged(int)), parentWidget(), SLOT(setFrame(int)));
	connect(this, SIGNAL(timeStepChanged(float)), parentWidget(), SLOT(setTimeStep(float)));
	connect(this, SIGNAL(particlesChanged(int)), parentWidget(), SLOT(setParticles(int)));
	connect(this, SIGNAL(velocitiesChanged(float, float)), parentWidget(), SLOT(setVelocities(float, float)));
	connect(this, SIGNAL(pressuresChanged(float, float)), parentWidget(), SLOT(setPressures(float, float)));
	start();
}

// timer
void GLWidget::simTimerUpdate() {
	update();
	if (running) {
		simTimer.start(0);
	}
}

void GLWidget::frameTimerUpdate() {
	updateFrame();
}

// functions
void GLWidget::start() {
	// 0 = sandbox
	// 1 = pressure test

	simulation = new Simulation();
	// particles
	int q = simulation->nx / 6;
	for (int x = simulation->nx / 2 - q; x <= simulation->nx / 2 + q; x++) {
		for (int y = 2; y < simulation->ny - 2; y++) {
			simulation->addParticle(x, y);
		}
	}
	simulation->updateCells();

	running = false;

	simTimer.setSingleShot(true);
	frameTimer.start(1000 / FPS);
}

void GLWidget::stop() {
	running = false;
	simTimer.stop();
	frameTimer.stop();
	simulation = 0;
}

void GLWidget::update() {

	// before update
	float dt = simulation->calculateTimeStep();

	// update
	simulation->update(dt);
	simulation->moveParticles(dt);

	// after update

	// gui
	frameChanged(simulation->n);
	timeChanged(simulation->t);
	pressuresChanged(simulation->minPressure, simulation->maxPressure);
	velocitiesChanged(simulation->minVelocity, simulation->maxVelocity);
	timeStepChanged(dt);
	int particles = simulation->particles.size();
	particlesChanged(particles);
}

void GLWidget::updateFrame() {
	repaint();
}

// input
void GLWidget::play() {
	running = true;
	simTimer.start(0);
}

void GLWidget::pause() {
	running = false;
	simTimer.stop();
}

void GLWidget::step() {
	update();
}

void GLWidget::sourceStatus(bool b) {
	simulation->activeSources = b;
}

void GLWidget::drainStatus(bool b) {
	simulation->activeDrains = b;
}

// gui
void GLWidget::restart() {
	stop();
	start();
}

void GLWidget::showGridVelocities(bool b) {
	_showGridVelocities = b;
}

void GLWidget::showParticleVelocities(bool b) {
	_showParticleVelocities = b;
}

void GLWidget::showParticleSpeed(bool b) {
	_showParticleSpeed = b;
}

void GLWidget::showGridPressures(bool b) {
	_showGridPressures = b;
}

void GLWidget::showParticlePressures(bool b) {
	_showParticlePressures = b;
}

void GLWidget::showGrid(bool b) {
	_showGrid = b;
}

void GLWidget::initializeGL() {
	glClearColor(0.2, 0.2, 0.2, 1);
	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHTING);
}

void GLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float scale = max(simulation->nx, simulation->ny + 1) / 2;
	glTranslatef(-1, -1, 0);
	glScalef(1.0 / scale, 1.0 / scale, 1);
	glTranslatef(0.3, 0.3, 0);

	// draw grid pressures
	if (_showGridPressures) {
		for (int i = 0; i < simulation->nx; i++) {
			for (int j = 0; j < simulation->ny; j++) {
				Cell * c = simulation->cell(i, j);

				float pressure = simulation->pressure(i, j);
				float minp = simulation->minPressure;
				float maxp = simulation->maxPressure;
				float h = 0;
				if (maxp != minp) {
					h = (pressure - minp) / (maxp - minp);
				}
				glColor3f(h, 1 - h, 0);
				fillRect(i - 0.5, j - 0.5, 1);
			}
		}
	}
	// draw grid
	if (_showGrid) {
		for (int i = 0; i < simulation->nx; i++) {
			for (int j = 0; j < simulation->ny; j++) {
				Cell * c = simulation->cell(i, j);
				if (c->type == Cell::FLUID) {
					glColor3f(0, 0, 1);
					drawRect(i - 0.5, j - 0.5, 1);
				}

			}
		}

		for (int i = 0; i < simulation->nx; i++) {
			for (int j = 0; j < simulation->ny; j++) {
				Cell * c = simulation->cell(i, j);
				if (c->function == Cell::DRAIN) {
					glColor3f(1, 0, 0);
					drawRect(i - 0.5, j - 0.5, 1);
				}
				if (c->function == Cell::SOURCE) {
					glColor3f(0, 1, 0);
					drawRect(i - 0.5, j - 0.5, 1);
				}
			}
		}	
		for (int i = 0; i < simulation->nx; i++) {
			for (int j = 0; j < simulation->ny; j++) {
				Cell * c = simulation->cell(i, j);
				// rect
				glColor3f(0.5, 0.5, 0.5);
				drawRect(i - 0.5, j - 0.5, 1);
				if (simulation->cell(i, j)->debug) {
					float w = 0.1;
					glColor3f(1, 1, 0);
					drawRect(i - 0.5 + w, j - 0.5 + w, 1 - 2 * w);
				}
			}
		}
	}

	// cells
	for (int i = 0; i < simulation->nx; i++) {
		for (int j = 0; j < simulation->ny; j++) {
			Cell * c = simulation->cell(i, j);

			// draw solids
			if (c->type == Cell::SOLID){
				glColor3f(0.5, 0, 0);
				fillRect(i - 0.5, j - 0.5, 1);
			}

			// draw vektors
			if (_showGridVelocities) {
				float vx = c->velocity.x * VELOCITY_DRAW_RATIO;
				float vy = c->velocity.y * VELOCITY_DRAW_RATIO;
				glColor3f(1, 0.27, 0);
				glBegin(GL_LINES);
				glVertex3f(i - 0.5, j - 0.5, 0);
				glVertex3f(i - 0.5 + vx, j - 0.5 + vy, 0);
				glEnd();
			}
		}

	}

	vector<Particle*> particles = simulation->particles;
	// calculating maximal values
	float maxs = -INFINITY;
	float mins = +INFINITY;
	for (Particle *p : particles) {
		float speed = abs(p->x - p->oldx) + abs(p->y - p->oldy);
		if (simulation->cell(p->x, p->y)->function != Cell::NONE) speed = mins;
		mins = min(mins, speed);
		maxs = max(maxs, speed);
	}

	// draw particles 
	for (Particle *p : particles) {
		// default color
		glColor3f(0, 0, 1);
		// pressure color
		if (p->debug) {
			glColor3f(1, 1, 0);
		}
		else if (_showParticleSpeed) {
			float speed = abs(p->x - p->oldx) + abs(p->y - p->oldy);
			float h = 0;
			if (maxs != mins) {
				h = (speed - mins) / (maxs - mins);
			}
			glColor3f(h, 0, 1 - h);
		}
		else if (_showParticlePressures) {
			float pressure = simulation->pressure(p->x, p->y);
			float minp = simulation->minPressure;
			float maxp = simulation->maxPressure;
			float h = 0;
			if (maxp != minp) {
				h = (pressure - minp) / (maxp - minp);
			}
			glColor3f(h, 1 - h, 0);
		}
		fillRect(p->x - 0.25, p->y - 0.25, 0.5);
	}

	// draw particles velocities
	for (Particle *p : particles) {
		glColor3f(1, 0.27, 0);
		if (_showParticleVelocities) {
			vec2 v = simulation->velocity(p->x, p->y);
			glBegin(GL_LINES);
			glVertex3f(p->x, p->y, 0);
			glVertex3f(p->x + v.x * VELOCITY_DRAW_RATIO, p->y + v.y * VELOCITY_DRAW_RATIO, 0);
			glEnd();
		}
	}

	glTranslatef(-0.3, -0.3, 0);
	glScalef(scale, scale, 1);
	glTranslatef(1, 1, 0);
}

void GLWidget::resizeGL(int w, int h) {
	glViewport(0, 0, h, w);
	glMatrixMode(GL_PROJECTION);
}

void GLWidget::fillRect(float x, float y, float w) {
	glBegin(GL_QUADS);
	glVertex3f(x, y, 0);
	glVertex3f(x + w, y, 0);
	glVertex3f(x + w, y + w, 0);
	glVertex3f(x, y + w, 0);
	glEnd();
}

void GLWidget::drawRect(float x, float y, float w) {
	glBegin(GL_LINES);
	glVertex3f(x, y, 0);			//1
	glVertex3f(x + w, y, 0);		//1

	glVertex3f(x + w, y, 0);		//2
	glVertex3f(x + w, y + w, 0);	//2

	glVertex3f(x + w, y + w, 0);	//3
	glVertex3f(x, y + w, 0);		//3

	glVertex3f(x, y + w, 0);		//4
	glVertex3f(x, y, 0);			//4
	glEnd();
}