#include "window.h"

#include <iostream>

Window::Window(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

void Window::setTime(float t) {
	ui.outputTime->setText(QString::number(t));
}

void Window::setFrame(int i) {
	ui.outputFrame->setText(QString::number(i));
}

void Window::setTimeStep(float t) {
	ui.outputTimeStep->setText(QString::number(t));
}

void Window::setParticles(int t) {
	ui.outputParticles->setText(QString::number(t));
}

void Window::setVelocities(float min, float max) {
	ui.outputMinVelocity->setText(QString::number(min));
	ui.outputMaxVelocity->setText(QString::number(max));
}

void Window::setPressures(float min, float max) {
	ui.outputMinPressure->setText(QString::number(min));
	ui.outputMaxPressure->setText(QString::number(max));
}